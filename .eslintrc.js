// http://eslint.org/docs/user-guide/configuring

module.exports = {
  "extends": [
    "plugin:vue/essential",
    "eslint:recommended"
  ],
  "rules": {
    "no-console": "off"
  },
  "parserOptions": {
    "parser": "babel-eslint",
    "sourceType": "module"
  }
}
